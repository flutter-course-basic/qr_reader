import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:qr_reader/models/scan_mode.dart';
export 'package:qr_reader/models/scan_mode.dart';

class DBProvider {
  static Database? _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database?> get database async {
    if (_database != null) return _database;
    _database = await initDB();
    return _database;
  }

  Future<Database?> initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'ScansDB.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute(''' 
        CREATE Table Scans(
          id INTEGER PRIMARY KEY,
          tipo TEXT,
          valor TEXT
        )
        ''');
    });
  }

  newScanRow(ScanModel newScan) async {
    final db = await database;
    final res = await db?.rawInsert('''
      INSERT Into Scans (id, tipo, valor)
      VALUES(${newScan.id}, '${newScan.tipo}', '${newScan.valor}')
    ''');
    return res;
  }

  Future<int?> newScan(ScanModel newScan) async {
    final db = await database;
    final res = await db?.insert('Scans', newScan.toMap());
    return res;
  }

  Future<ScanModel?> getScanById(int id) async {
    final db = await database;
    final res = await db?.query('Scans', where: 'id = ?', whereArgs: [id]);

    return res!.isNotEmpty ? ScanModel.fromMap(res.first) : null;
  }

  Future<List<ScanModel?>> getAllScans() async {
    final db = await database;
    final res = await db?.query('Scans');

    return res!.isNotEmpty
        ? res.map((scan) => ScanModel.fromMap(scan)).toList()
        : [];
  }

  Future<List<ScanModel?>> getAllScansByTipo(String? tipo) async {
    final db = await database;
    final res = await db?.rawQuery('''
      SELECT * FROM Scans
      WHERE tipo = '$tipo'
''');

    return res!.isNotEmpty
        ? res.map((scan) => ScanModel.fromMap(scan)).toList()
        : [];
  }

  Future<int?> updateScan(ScanModel newScan) async {
    final db = await database;
    final res = await db?.update('Scans', newScan.toMap(),
        where: 'id = ?', whereArgs: [newScan.id]);
    return res;
  }

  Future<int?> deleteScan(int id) async {
    final db = await database;
    final res = await db?.delete('Scans', where: 'id = ?', whereArgs: [id]);
    return res;
  }

  Future<int?> deleteAllScans() async {
    final db = await database;
    final res = await db?.rawDelete('''
      DELETE FROM Scans
    ''');
    return res;
  }
}

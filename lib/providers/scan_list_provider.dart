import 'package:flutter/material.dart';
import 'package:qr_reader/providers/db_provider.dart';

class ScanListProvider extends ChangeNotifier {
  List<ScanModel?> scans = [];
  String tipoSeleccionado = 'http';

  Future<ScanModel> nuevoScan(String valor) async {
    final nuevoScan = ScanModel(valor: valor);
    final id = await DBProvider.db.newScan(nuevoScan);
    nuevoScan.id = id;

    if (tipoSeleccionado == nuevoScan.tipo) {
      scans.add(nuevoScan);
      notifyListeners();
    }

    return nuevoScan;
  }

  cargarScan() async {
    scans = [];
    scans = await DBProvider.db.getAllScans();
    notifyListeners();
  }

  cargarScanPorTipo(String tipo) async {
    scans = [];

    tipoSeleccionado = tipo;
    scans = await DBProvider.db.getAllScansByTipo(tipo);
    notifyListeners();
  }

  borrarTodos() async {
    await DBProvider.db.deleteAllScans();
    scans = [];
    notifyListeners();
  }

  borrarScabPorId(int id) async {
    await DBProvider.db.deleteScan(id);
    scans.removeWhere((element) => element?.id == id);
    notifyListeners();
  }
}

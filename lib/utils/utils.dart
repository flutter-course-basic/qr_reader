import 'package:flutter/material.dart';
import 'package:qr_reader/providers/db_provider.dart';
import 'package:url_launcher/url_launcher.dart';

openUrl(BuildContext ctx, ScanModel? scan) async {
  final url = scan?.valor;

  if (scan?.tipo == 'http') {
    final Uri _url = Uri.parse(url!);
    if (!await launchUrl(_url)) throw 'Could not launch $url';
  } else {
    Navigator.pushNamed(ctx, 'mapa', arguments: scan);
  }
}

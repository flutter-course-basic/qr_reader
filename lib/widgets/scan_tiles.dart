import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_reader/providers/scan_list_provider.dart';
import 'package:qr_reader/utils/utils.dart';

class ScanTiles extends StatelessWidget {
  const ScanTiles({Key? key, required this.tipo}) : super(key: key);

  final String tipo;

  @override
  Widget build(BuildContext context) {
    final scanListProvider = Provider.of<ScanListProvider>(context);
    final scans = scanListProvider.scans;

    return ListView.builder(
      itemCount: scans.length,
      itemBuilder: (_, i) => Dismissible(
        key: UniqueKey(),
        direction: DismissDirection.endToStart,
        background: Container(color: Colors.red),
        onDismissed: (direction) {
          scanListProvider.borrarScabPorId(scans[i]?.id as int);
        },
        child: ListTile(
          leading: Icon(
              tipo == 'geo' ? Icons.map_outlined : Icons.home_outlined,
              color: Theme.of(context).primaryColor),
          title: Text(scans[i]?.valor as String),
          subtitle: Text(scans[i]?.id.toString() as String),
          trailing: const Icon(Icons.keyboard_arrow_right, color: Colors.grey),
          onTap: () => openUrl(context, scans[i]),
        ),
      ),
    );
  }
}
